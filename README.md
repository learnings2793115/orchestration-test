# Orchestration-test

## Getting started with ansible
```bash
```

## Getting started with docker compose

```bash
docker compose up -d
docker container ls -a
docker compose up --scale web=3 -d
docker container ls -a

curl http://localhost:80
```

## Getting started with terraform
```bash
terraform init
terraform apply
docker container ls -a
curl http://localhost:8000
```